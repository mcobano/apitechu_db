package com.techu.apitechu_db.services;

import com.techu.apitechu_db.models.ProductModel;
import com.techu.apitechu_db.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    /* Enganche Beans, al repository que pasa el contrato de metodos y aqui
        en servicio, pones la implementacion de la misma
    */
    @Autowired
    ProductRepository productRepository;


    public List<ProductModel> findAll(){
        System.out.println("Findlall en product service");
        return this.productRepository.findAll();
    }

    public ProductModel addProduct(ProductModel product){
        System.out.println("add Product en ProductService");
        // Podemos  usar esta return (this.productRepository.save(product));
        // o Pedemos usar el insert tb
        return (this.productRepository.insert(product));
    }

    // El optional está pensado para manejar el null y existencia de objeto como
    // retorno pero aunque lo puedes poner como param no esta pensado para eso.
    public Optional<ProductModel> findById (String id) {
        System.out.println("Find by ID prodcut Service");
        return this.productRepository.findById(id);
    }


    // Curioso que hace lo mismo que el addProduct , pero NUNCA hacerlo en pro
    // Cada servico su funcion especifica para evitar efecto lateral
    public ProductModel update (ProductModel product){
        System.out.println("Update (PUT) by ID prodcut Service");
        return (this.productRepository.save(product));
    }

    public boolean delete(String id){

        // no reutilizar la funcion del servicio es mejor hacer por le servicio
        // this.productRepository.findById(id) y hay que usar esta
        // this.findById(id).isPresent()
        System.out.println("Delete prodcut Service");
        boolean result = false;

        if ( this.findById(id).isPresent() == true) {
            this.productRepository.deleteById(id) ;
            result = true;
        }
        return result;
    }
}
