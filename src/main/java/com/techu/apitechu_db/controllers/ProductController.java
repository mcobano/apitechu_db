package com.techu.apitechu_db.controllers;

import com.techu.apitechu_db.models.ProductModel;
import com.techu.apitechu_db.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")

public class ProductController
{
    @Autowired
    ProductService productService;

    @GetMapping("/")
    public void pageIndex(){
        System.out.println("PAGINA INDICE");
    }

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts(){
        System.out.println("getProducts");
        return new ResponseEntity<>(this.productService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/products/{id}")
    // DEberia ser Product Model pero necesitamos el objct para devolver el string de "Producto no ecncontrao"
    public ResponseEntity<Object> getProductById(@PathVariable String id){
        System.out.println("get PRoduct by ID");
        System.out.println("La id del prod a buscar es "+ id);

        Optional<ProductModel> result = this.productService.findById(id);

        // para manejar objetos nulos usamos este result esot se puede con optional desde JAVA 8
        // empty true si vacio, Present es lo contrario deende de la version de java puedes usar uno o otro
        // get saca lo qye haya dentro de la "caja" optional, si quiers algo fino tiene el orElseGEt y otros methods.

        // Aqui tenemos 2 casos, 1- el objeto y si quieres poner el objeto o si no se encontró
        // el Http cambio , asamos ternario y sacamos el estatus correcto.
        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado" ,
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }


    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){
        System.out.println("addProduct");

        System.out.println("id es" + product.getId());
        System.out.println("desc es " + product.getDesc());
        System.out.println("price es " + product.getPrice());

        return new ResponseEntity<>(this.productService.addProduct(product),HttpStatus.CREATED);

    }

    @PutMapping ("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product ,@PathVariable String id){
        System.out.println("updateProduct");
        System.out.println("updateProduct para el id de la URL " + id);
        System.out.println("updateProduct para el id producto " + product.getId());
        System.out.println("updateProduct para el desc producto " + product.getDesc());
        System.out.println("updateProduct para el price producto " + product.getPrice() );

        // A qui hcemos una variante paa no usar el Object insdiscrimniadamente
        Optional<ProductModel> productToUpdate = this.productService.findById(id);

        if ( productToUpdate.isPresent() ) {
            System.out.println("Producto para actualizar encontrado");
            this.productService.update(product);
        }

        return new ResponseEntity<ProductModel>(product ,
                productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);

    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){
        System.out.println("DeleteProduct");
        System.out.println("DeleteProduct a borrar id " + id);

        Boolean deletedProduct = this.productService.delete(id);

        return new ResponseEntity<String>(
                deletedProduct ? "Producto Borrado" : "Producto NO barrado",
                deletedProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
}

