package com.techu.apitechu_db.repositories;

import com.techu.apitechu_db.models.ProductModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends MongoRepository<ProductModel,String> {

}
