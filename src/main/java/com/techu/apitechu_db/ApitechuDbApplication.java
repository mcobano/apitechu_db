package com.techu.apitechu_db;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApitechuDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApitechuDbApplication.class, args);
	}

}
